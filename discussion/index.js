// this require("express") allows devs to load/import express package that will be used for the application

let express = require("express")

// express() - allows devs to create an application using express

const app = express();
//this code creates an express applpication and stores it inside the "app" variable. thus, "app" is the server already.

 // use function lets the middleware to do common services and capabilities to the applications
app.use(express.json()); // let the app to read json data
app.use(express.urlencoded({extended: true})); //allows the app to receive data from the forms
// not using these will return our body undefined

const port = 3000;

app.get("/", (req, res)=> {
	res.send("Hello World")
})

app.get("/hello", (req, res)=> {
	res.send("Hello from the /hello endpoint")
})

app.post("/hello", (req, res)=> {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})


/*ACTIVITY*/

let mockData = [
	{
		username: 'AA0001',
		password: 'Password1'
	},
	{
		username: 'AA0002',
		password: 'Password2'
	},
	{
		username: 'AA0003',
		password: 'Password3'
	},
	{
		username: 'AA0004',
		password: 'Password4'
	}
];


app.get("/home", (req, res)=> {
	res.send("Welcome to the home page")
});

app.get("/users", (req, res)=> {
	res.send(mockData)
});

app.delete("/delete-user", (req, res)=> {
	res.send(`User ${req.body.username} has been deleted`)
});

app.post("/signup", (req, res) => {
	let username = req.body.username
	let password = req.body.password
	
	if (username !== "" && password !== "") {
		mockData.push(req.body)
		res.send(`User ${req.body.username} successfully registered`)
	} else {
		res.send(`Please input both username and password`)
	}
});


app.listen(port,() => console.log(`Server is running at port ${port}`));


